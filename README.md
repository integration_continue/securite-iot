# Sécurité iot
**Rapport de Reverse Engineering**


**Description du Projet :**
Ce dépôt documente le processus de reverse engineering réalisé sur un objet connecté dans le cadre d'un projet visant à comprendre et à interagir avec ses fonctionnalités non documentées. L'objectif principal était de parvenir à envoyer un numéro récupérer depuis un smartphone vers une caractéristique de l'objet afin de répertorier les fichiers qu'il contient.

**Méthodologie :**
1. **Analyse de l'Objet :** L'examen initial de l'objet connecté a révélé plusieurs caractéristiques non définies, suggérant une fonctionnalité sous-utilisée ou non documentée. Ces caractéristiques ont été explorées pour identifier toute corrélation potentielle avec les fichiers stockés sur l'objet.

2. **Développement du Code Python :** À l'aide de la bibliothèque Bleak et des outils de communication Bluetooth, un script Python a été élaboré pour établir une connexion avec l'objet connecté et interagir avec ses caractéristiques. Le code a été conçu pour transmettre un numéro depuis un smartphone vers une caractéristique spécifique de l'objet.

3. **Extraction des Fichiers :** Une fois le numéro transmis avec succès, des techniques de reverse engineering ont été utilisées pour comprendre le protocole de communication entre le smartphone et l'objet. Cela a permis de développer un processus pour extraire les fichiers stockés sur l'objet et les enregistrer localement.

4. **Vérification de l'Intégrité des Fichiers :** Pour garantir l'intégrité des données récupérées, une vérification du hash des fichiers a été effectuée, confirmant ainsi leur intégrité. De plus, les tailles des fichiers ont été vérifiées pour s'assurer de l'exhaustivité de la récupération.

**Résultats :**
Le processus de reverse engineering a permis de comprendre et d'exploiter efficacement les fonctionnalités non documentées de l'objet connecté. Le script Python développé a facilité la transmission de données depuis un smartphone vers l'objet, permettant ainsi la récupération réussie des fichiers stockés. La vérification du hash et des tailles des fichiers a confirmé l'intégrité des données récupérées.

**Conclusion :**
Le reverse engineering de l'objet connecté a permis d'explorer et d'exploiter ses fonctionnalités cachées, ouvrant ainsi de nouvelles possibilités d'interaction avec l'appareil. Ce processus a démontré l'efficacité de l'approche de reverse engineering pour découvrir et comprendre les aspects non documentés des dispositifs connectés, tout en soulignant l'importance de la vérification de l'intégrité des données extraites.

**Structure du Dépôt :**
- `Code_final_de_la_pocédure`: Contient le script Python utilisé pour l'interaction avec l'objet connecté.
- `documentation/`: Comprend toute documentation supplémentaire liée au projet. // à faire encore ....
- `README.md`: Fournit un aperçu du projet, de la méthodologie, des résultats et de la conclusion.

**Instructions d'Utilisation :**
1. Clonez le dépôt sur votre machine locale.
2. Exécutez le script Python à l'aide d'un interpréteur Python compatible.

**Contributions :**
Les contributions visant à améliorer le code ou la documentation sont les bienvenues. Veuillez forker le dépôt, apporter vos modifications et soumettre une demande de fusion pour examen.

