import asyncio
from bleak import BleakScanner

async def scan():
    scanner = BleakScanner()
    devices = await scanner.discover()
    for device in devices:
        print(device)

asyncio.run(scan())
