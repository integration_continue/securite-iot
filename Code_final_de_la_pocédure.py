import asyncio
from bleak import BleakClient, BleakScanner
import time
import struct
async def device(mac):
    # scan for devices
    devices = await BleakScanner.discover()
    for d in devices:
        if d.address == mac:
            return d
    return None


def getNotified(sender,data):
    
    #nom, size, hash, creationdate = struct.unpack('8sI4sQ', data)
    print(data)
    with open("T11.hex", "ab") as file:
            file.write(data)
# Affichage des données
    #print(f"Nom: {nom.decode('utf-8')}")
    #print(f"Size: {size}")
    #print(f"Hash en hex : {hash.hex()}")
    #print(f"Creation Date: {creationdate}")

async def main():
    d = await device('C7:20:E0:76:6E:1B')
    async with BleakClient(d.address) as client:
        if client.is_connected:
            ref = {}
            numValue = 0
            print('connected')
            services = await client.get_services()
            for service in services:
                for charac in service.characteristics:
                    temp = str(str(charac) + " UNKNOWN").split()
                    ref[temp[0] + '-' + temp[3]] = temp[1:][1][:-2]
            

                    
            for key in ref:
                if key.split('-')[-1] == 'LIST' or key.split('-')[-1] == 'READ':
                    await client.start_notify(int(ref[key]), getNotified)

            
            # Write to the LIST characteristic first
            
            for key in ref:
                if key.split('-')[-1] == 'NUM':
                    numValue = await client.read_gatt_char(int(ref[key]))
            for key in ref:
                if key.split('-')[-1] == 'LIST':
                    
                    await client.write_gatt_char(int(ref[key]), numValue)
            

            
            for key in ref:
                if key.split('-')[-1] == 'READ':
                    
                    await client.write_gatt_char(int(ref[key]), b"T11\x00\x00\x00\x00\x00\x00\x00\x00\x00\x27\x0F\x00\x00")
            time.sleep(3)
           
            
asyncio.run(main())


