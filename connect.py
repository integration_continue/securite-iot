import asyncio
from bleak import BleakClient

address = "C7:20:E0:76:6E:1B"

async def main(address):
    client = BleakClient(address)
    try:
        await client.connect()
        services = await client.get_services()
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for characteristic in service.characteristics:
                print(f"  Characteristic UUID: {characteristic}")
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

asyncio.run(main(address))
