import binascii

def calculate_crc32(filename):
    try:
        with open(filename, 'rb') as f:
            buf = f.read()
        return binascii.crc32(buf)
    except FileNotFoundError:
        return "Le fichier n'a pas été trouvé."

# Utilisation de la fonction
filename = "T13.hex"  
print("T13")
print(f"CRC32: {calculate_crc32(filename):08X}")

print("T11")
filename = "T11.hex"  
print(f"CRC32: {calculate_crc32(filename):08X}")

print("T10")
filename = "T10.hex"  
print(f"CRC32: {calculate_crc32(filename):08X}")

print("T9780")
filename = "T9780.hex"  
print(f"CRC32: {calculate_crc32(filename):08X}")
